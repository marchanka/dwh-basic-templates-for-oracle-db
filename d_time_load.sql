DROP TABLE D_TIME;

CREATE TABLE D_TIME
  (
 -- MINUTES
     D_TIME_ID            NUMBER DEFAULT -1 NOT NULL,
     MINUTE_NR            NUMBER,
     MINUTE_OF_HOUR       VARCHAR2(2),
     MINUTE_OF_HOUR_DESCR VARCHAR2(10),
 -- HOUR
     HOUR_OF_DAY_24       VARCHAR2(2),
     HOUR_OF_DAY_24_DESCR VARCHAR2(20),
     HALF_HOUR            VARCHAR2(1),
     HOUR_OF_DAY_12       VARCHAR2(2),
     AM_PM                VARCHAR2(2),
     CONSTRAINT PK_D_TIME PRIMARY KEY (D_TIME_ID)
  )
  TABLESPACE &1;

---- GRANTS
GRANT SELECT, UPDATE, INSERT ON D_TIME TO ME;

---- SYNONYM
CREATE OR REPLACE PUBLIC SYNONYM D_TIME FOR D_TIME;

---- COMMENTS
   COMMENT ON TABLE D_TIME IS 'Context: central time table. Content: table contains time information on the minutes level.';

   COMMENT ON COLUMN D_TIME.D_TIME_ID IS 'Primary key of table';
   COMMENT ON COLUMN D_TIME.MINUTE_NR IS '0 till 1439';
   COMMENT ON COLUMN D_TIME.MINUTE_OF_HOUR IS '00 till 59 every hour';
   COMMENT ON COLUMN D_TIME.MINUTE_OF_HOUR_DESCR IS '00 - 01 till 59 - 00 every hour';
   COMMENT ON COLUMN D_TIME.HOUR_OF_DAY_24 IS '00 till 23';
   COMMENT ON COLUMN D_TIME.HOUR_OF_DAY_24_DESCR IS '00:00 - 01:00 till 23:00 - 00:00';
   COMMENT ON COLUMN D_TIME.HALF_HOUR IS '1 and 2 every hour';
   COMMENT ON COLUMN D_TIME.HOUR_OF_DAY_12 IS '12 AM, 1 AM then till 11 PM';
   COMMENT ON COLUMN D_TIME.AM_PM IS 'AM/PM';

COMMIT;


---- LOAD

insert into d_time
(d_time_id, minute_nr, minute_of_hour, minute_of_hour_descr, hour_of_day_24, hour_of_day_24_descr, half_hour, hour_of_day_12, am_pm)
(
select
  t.d_time_id,
  t.minute_nr,
  case when t.minute_of_hour<10 then '0'||t.minute_of_hour else ''||t.minute_of_hour end as minute_of_hour,
  (case when t.minute_of_hour<10 then '0'||t.minute_of_hour else ''||t.minute_of_hour end) || ' - ' ||
  (case when t.minute_of_hour_n<10 then '0'||t.minute_of_hour_n else ''||t.minute_of_hour_n end) || ''
    as minute_of_hour_descr,
  case when t.hour_of_day_24<10 then '0'||t.hour_of_day_24 else ''||t.hour_of_day_24 end as hour_of_day_24,
  (case when t.hour_of_day_24<10 then '0'||t.hour_of_day_24 else ''||t.hour_of_day_24 end) || ':00 - ' ||
  (case when t.hour_of_day_24_n<10 then '0'||t.hour_of_day_24_n else ''||t.hour_of_day_24_n end) || ':00'
    as hour_of_day_24_descr,
  t.half_hour,
  t.hour_of_day_12,
  t.am_pm
from (
SELECT n as d_time_id
      , n-1 AS minute_nr
      , mod(n-1,60) AS minute_of_hour
      , case when mod(n-1,60)+1=60 then 0 else mod(n-1,60)+1 end AS minute_of_hour_n
      , trunc((n-1)/60) as hour_of_day_24
      , case when trunc((n-1)/60)+1=24 then 0 else trunc((n-1)/60)+1 end as hour_of_day_24_n
      , case when trunc((n-1)/60)=0 then 12 when trunc((n-1)/780)=0 then trunc((n-1)/60) else trunc((n-1)/60)-12 end as hour_of_day_12
      , case when trunc((n-1)/60)<12 then 'AM' else 'PM' end as am_pm
      , case when mod(trunc((n-1)/30),2)=0 then 1 else 2 end AS half_hour
FROM (
SELECT LEVEL n
      FROM DUAL
CONNECT BY LEVEL <= 1440)
) t );

COMMIT;
